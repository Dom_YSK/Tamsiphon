The city has many guilds, adventurer's guilds supplement the guards, merchant companies gather in their guild halls, 
crafters of all kinds group together as guilds... and so do the thieves.

The most powerful guilds are:

* The West Oceanic trading guild, which controls and owns most of the docklands and shipping in and out of the harbour. 
They bring exotic foods, fish and silks from across the oceans.
* The Desert Trail trading guild, which has outposts along the trade route to the west, allowing merhcants shelter, safety
and respite on their journey, for a price. They also deal strongly in salt, silks and luxury goods from the markets of
Waterdeep.
* The Farmer's guild, which is surprisingly powerful. Strong legal protection for the rights of small farm owners and an 
absolute ban on building on fertile ground has meant that the small holders around the city are secure enough that they
provide all the food the city needs. This has also given them enough weight in the city that the new wall is being built 
around their lands, offering protection from the raiders from the North.
* The Adventurer's guild. Derided as often as they are used by the Merchants as nothing but mercenaries, this is where 
most foreign soldiers and fortune seekers end up. Very few people will hire outside the adventurers guild and when they 
do they hire...
* The Thieves Guild: Technically there is only one thieves guild in Tamsiphon, headed by a seemingly powerless but 
astute elf who holds the post of "Thiefmistress". However there are two major factions which appear to be completely 
separate guilds within the Theives guild. The Light Bazaar and the shallowest layers of Downtown are run by the light
hands, a group which is disproportionately Elven and Human. The Red eyes run Downtown proper and the Dark Bazaar. There 
are constant skirmishes where these territories meet. 