It’s a D&D type world based in an ancient Persia-a-like, which will be set in a river valley, mainly, somewhere in a world where mythology is real . 

Guilds would be nice, perhaps? 

There’s a shah-a-like, perhaps? Don’t use the name Caliph? Probably something better like shah? Are there any non religious ruler names that are uniquely middle eastern? 

There will also be a desert to the west of the river valley, and mountains to the north and east. The northern mountains are heavily forested with pine and cypress, the east is more barren, but beyond the mountains lies a steppe. 

The city is the Nexus of trade routes, barbarians (visigoths?) from the east come with amber and furs  while from across the desert there come Oriental types with silk, tea and spices . To the north live the black skinned people and over the southern sea? Who knows? 

The city itself is ancient and is built on the remains of earlier cities, or rather iterations of itself. One must be wary in the Warren of tunnels under it, as ancient magical experiments and more recent monsters can lurk down there. 

The city itself has an efficient and well paid guard, their families are treated to the best education and housing while they work for the city, but not beyond. Thus retirees and their families are kicked out… unless a child takes their place in the city guard. They must still pass the tests, which are open to all. The city guard exists somewhere between police and paramilitary strike force. They break up tavern fights and fend off zombie attacks.  However they have their limits and they're not averse to hiring adventures when they need more muscle or specialist skills. 

The city also has the guilds, from the merchants to the adventurers. They are all potent political forces, and military forces too. Should the guilds unite, they'd have twice the raw manpower of the city guard. Much of the efforts of the shah’s political cadre is to keep the guilds fighting. 

There is also a a school of wizardry, it occupies a large grassy estate in the north of the city, and much of the tunnels in that area to a surprising depth. It's rumoured that the wizards have dug down to hell where they get their powers from. 

The temples and churches of the God's faces, the aspects of war, love, charity, death and so on take up the avenue from the dock to the royal Palace in the centre of the city. 

The city has a massive bazaar, with some stalls that are old and gigantic, reaching multiple storeys often. The city passed an edict under a mad monarch a thousand years earlier stating that buying and selling must take place in the open  in the view of God. This law has never been repealed. The merchants managed to make it so during the transition. 

The government itself is organised along guild lines, the tax collectors, the road maintainers, the teachers (education is free for children and tax supported for primary education). 

The surrounding area is mostly pacified, but adventurers can often get free passage with trade caravans or ships in return for protection from pirates and bandits. 

In the desert there live wandering nomadic monks who dedicate themselves to the god of justice, the sun God. They can channel his divine wrath against those who have committed sins. 

The mountains are largely settled by people from the city, mostly worshippers of the nature God of balance. It's said that at the equinoxes these people have secret rites in the forest where they reaffirm themselves to the balance. 

The seas are full of islands nearby,which have become suburbs of the city, with regular ferries and bridges between the closer islands. 

The city main harbour is actually in the estuary for the river. The river's source is in the forested mountains and it cuts right by the bazaar. Many merchants there operate their own docks and ships. The city's tax collectors can't patrol it all the time, nor inspect every cargo. Smuggling is rife, of people as well as of goods. 

The citizens of the city are human for the most part, though efreeti and djinn are not unknown. The wizards quarter is often host to extraplanar entities and the tunnels under the city have been known to bring forth strange chimaera and play host to hybrids from beyond the walls. In the periphery of the tunnel system, passages to natural cave systems have opened or been opened by means both natural and sinister. A good rule is that the lower you go, the more likely people are to be not human. 

The upper levels of the city’s tunnel systems are basically subterranean streets and houses, with the latest level being open to the sky in some places, Every few generations the river has a megaflood and dumps several metres of silt onto the plain around the city. This silt is excellent fertiliser and  will, as years go by, be “mined” from the city’s old streets, clearing the sub path. However, the city needs to function and shifting metres of silt from every street and surround would take too long, so now houses are built with magically braced walls, from which arches, buttresses and a new street can be erected quickly. Inside the city itself, the weight of the city has slowly moved the magically strong buildings into the earth itself. This means that by and large, the city remains at the same level as always, the periodic flooding essentially replacing what’s lost by erosion in the fields around the city. 

Due to its… unique nature, sewers of the traditional type are not possible, so the city is serviced by nightsoilmen in the poorer areas and coproducts in the richer areas. The common joke is that the more money you have, the higher your shit is. 

The farmers around the city are generally freeholders, their land is guaranteed by royal charter for the provision of food to the city, and with the new walls will be guarded by the city in times of distress. However, there may be times when you would want to build housing on the farmland to make a quick profit. To prevent this the taxes on building a house on farm land are incredibly high. It is still done by some of the richer nobles, as a mark of how rich they are, but for the most part, the farmers still own their land and the city can be self sufficient. 

This means the city is not expanding outwards as other cities do, but down. The lower reaches of the city’s tunnels are the poorest, but people are always looking for even deeper dwellings. The gross geography of the city has changed over the centuries and with the exception of the royal palace and the school of wizardry, what was a poor quarter today was a palace three hundred years ago. There are forgotten storerooms and old treasures hidden in the ruins of previous generations. Many a sub-dweller has become a prosperous merchant off the back of a discovery in the old city. Everything from magical weapons and armour to books of forgotten lore and strongrooms of gold and jewels have been found in the tunnels. 

Only the latest level still has silt left for mining in, and so  the lower levels, magically strengthened and sealed against damp, are largely empty warrens where humans do not tread, and gangs of non-humans dwell, occasionally making raids against the upper lands to steal supplies. Food is not hard to find in the deeps, only killing it before it kills you is. 


Most adventurers start their careers helping to clear out another layer of tunnels and driving off the local monsters that dwell there. Close to the surface, the monsters are weak and often being controlled by stronger monsters deep down. It’s rumoured that the very first city had a wizard prince who refused to leave his quarters during the flood and who tied his life to the life of the city itself. Now, trapped down in the depths he uses his great magic and millennia of experience to control all the deeps inhabitants, and a few of the surface dwellers. 

The sea trade is bountiful, with traders from across the world’s oceans arriving daily. The archipelago, the bridges and the large harbour have made besieging the city a difficult proposition at best. However sometimes the city has fallen on hard times during a siege because its food supply has been cut off as occupiers camped outside the walls. The new wall hopes to combat that, with the palace buying and clearing all land outside the walls and up to the new walls and reserving it as farmland. 


The tunnels are called downtown. 
